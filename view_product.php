<?php 

include "includes/header.php";
include "dbcon.php";

?>


<?php 

if(isset($_GET['product_id'])) {
    $the_product_id = mysqli_real_escape_string($connect, $_GET['product_id']);
}

?>


<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>
                        Full Info
                        <a href="index.php" class="btn btn-danger float-end">Go Back</a>
                    </h4>
                </div>
                <div class="card-body">

                <?php 
                
                $query = "SELECT * FROM products WHERE product_id = $the_product_id";
                $query_run = mysqli_query($connect, $query);

                while($row = mysqli_fetch_assoc($query_run)) {
                    $product_id = $row['product_id'];
                    $title = $row['title'];
                    $price = $row['price'];
                    $description = $row['description'];
                    $category = $row['category'];
                    $image = $row['image'];
                }

                ?>

                <div class="mb-3">
                    <label>Title</label>
                    <p class="form-control">
                        <?php echo $title; ?>
                    </p>
                </div>
                <div class="mb-3">
                    <label>Price</label>
                    <p class="form-control">
                        <?php echo $price; ?>
                    </p>
                </div>
                <div class="mb-3">
                    <label>Description</label>
                    <p class="form-control">
                        <?php echo $description; ?>
                    </p>
                </div>
                <div class="mb-3">
                    <label>Category</label>
                    <p class="form-control">
                        <?php echo $category; ?>
                    </p>
                </div>
                <div class="mb-3">
                    <label>Image </label>
                    <img width="200" height="200" class="img-fluid" src="<?php echo $image; ?>">   
                </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php 

if(isset($_POST['create_review'])) {
$the_product_id = $_GET['product_id'];

$review_author = $_POST['review_author'];
$review_content = $_POST['review_content'];

if(!empty($review_author) && !empty($review_content)) {
    $query = "INSERT INTO reviews (review_product_id, review_author, review_content, review_date) VALUES ($the_product_id, '{$review_author}', '{$review_content}', NOW())";
    $create_review = mysqli_query($connect, $query);

    if(!$create_review) {
        die("Review Query Failed". mysqli_error($connect));
    }

    $query = "UPDATE products SET product_review_count = product_review_count + 1 WHERE product_id = $the_product_id";
    $update_product_count = mysqli_query($connect, $query);
} else {
    echo "<script>alert('Fields Cannot be Empty')</script>";
}
}

?>

<div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card-header"><h4>Leave a review:</h4></div>
                    <div class="card-body">
                    <form action="" method="post" role="form">

                        <div class="form-group">
                            <label for="Author">Author</label>
                            <input type="text" class="form-control" name="review_author">
                        </div>

                        <div class="form-group">
                            <label for="Comment">Comment</label>
                            <textarea class="form-control" name="review_content" rows="3"></textarea>
                        </div>
                        <button type="submit" name="create_review" class="btn btn-primary mt-3">Submit</button>
                    </form>
                    </div>
                </div>
             </div>            
    </div>  

    <?php 
    
    $query = "SELECT * FROM reviews WHERE review_product_id = {$the_product_id} ORDER BY review_id DESC";
    $select_reviews_query = mysqli_query($connect, $query);

    if(!$select_reviews_query) {
        die("Query Failed". mysqli_error($connect));
    }
    
    while($row = mysqli_fetch_assoc($select_reviews_query)) {
        $review_date = $row['review_date'];
        $review_author = $row['review_author'];
        $review_content = $row['review_content'];
    
    
    ?>

<div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card-header"><h4><?php echo $review_author; ?></h4></div>
                    <div class="card-body">
                    <form action="" role="form">
                        <div class="form-group">
                            <label for="Date"><?php echo $review_date; ?></label>
                        </div>

                        <div class="form-group">
                            <textarea disabled class="form-control" name="review_content" rows="3"><?php echo $review_content; ?></textarea>
                        </div>
                    </form>
                    </div>
                </div>
             </div>            
    </div>        
   <?php }
    
    ?>



<?php 

include "includes/footer.php";

?>