<?php 

include "dbcon.php";

function isLoggedIn() {
    if(isset($_SESSION['username'])) {
        return true;
    }

    return false;
}

function query($query){
    global $connect;
    return mysqli_query($connect, $query);
}

//Function to get logged in user ID

function loggedInUser() {
    $result = query("SELECT * FROM users WHERE username='".$_SESSION['username']."'");
    $user = mysqli_fetch_array($result);

    return mysqli_num_rows($result) >= 1? $user['user_id'] : false;
}

//Function to check whether logged in user favorited certain product

function userFavoritedThis($product_id = '') {
    $result = query("SELECT * FROM favorites WHERE user_id = ".loggedInUser()." AND product_id = {$product_id}");
    return mysqli_num_rows($result);
}

?>