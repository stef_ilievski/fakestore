<?php 

include "includes/header.php";
include "functions.php";
session_start();

if(!isset($_SESSION['username'])) {
    header('Location: login.php');
}


?>

<?php 

if(isset($_POST['favorite'])) {
    $product_id = $_POST['product_id'];
    $user_id = $_POST['user_id'];

    //1. SELECT PRODUCT 

    $query = "SELECT * FROM products WHERE product_id = $product_id";
    $query_run = mysqli_query($connect, $query);
    $queryResult = mysqli_fetch_array($query_run);
    $favorites = $queryResult['favorites'];

    //2. UPDATE PRODUCTS WITH FAVORITES

    mysqli_query($connect, "UPDATE products SET favorites=$favorites+1 WHERE product_id = $product_id");

    //3. INSERT/INCREMENT FAVORITES FOR PRODUCTS 

    mysqli_query($connect, "INSERT INTO favorites (user_id, product_id) VALUES($user_id, $product_id)");
    exit();
}

if(isset($_POST['unfavorite'])) {
    $product_id = $_POST['product_id'];
    $user_id = $_POST{'user_id'}; // mozhe i so php echo loggedinuser 
    //1. SELECT PRODUCT 

    $query = "SELECT * FROM products WHERE product_id = $product_id";
    $query_run = mysqli_query($connect, $query);
    $queryResult = mysqli_fetch_array($query_run);
    $favorites = $queryResult['favorites'];

    //2. DELETE FAVORITES 

    mysqli_query($connect, "DELETE FROM favorites WHERE product_id = $product_id AND user_id = $user_id");

    //3. UPDATE WITH DECREMENT FAVORITES 

    mysqli_query($connect, "UPDATE products SET favorites=$favorites-1 WHERE product_id = $product_id");
    exit();
}

?>


<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="text-center"> Welcome: <?php echo $_SESSION['username']; ?>
                        <a href="logout.php" class="btn btn-dark float-end">Log Out</a>
                    </h4>
                </div>
                <div class="card-header">
                    <h4 class="text-left"> Product Details
                    </h4>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-dark table-hover">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Price</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Image</th>
                                <th>View info</th>
                                <th>Reviews</th>
                                <th>Favorites</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $query = "SELECT * FROM products";
                            $select_products = mysqli_query($connect, $query);

                            while($row = mysqli_fetch_assoc($select_products)) {
                                $product_id = $row['product_id'];
                                $title = $row['title'];
                                $price = $row['price'];
                                $description = $row['description'];
                                $category = $row['category'];
                                $image = $row['image'];
                                $reviews = $row['product_review_count'];


                                echo "<tr>";
                                echo "<td>{$title}</td>";
                                echo "<td>{$price}$</td>";
                                echo "<td>{$description}</td>";
                                echo "<td>{$category}</td>";
                                echo "<td><img src='{$image}' width='100' height='100' class='img-fluid'</td>";
                                echo "<td><a href='view_product.php?product_id={$product_id}' class='btn btn-info btn-sm'>View</a></td>";
                                echo "<td>{$reviews}</td>";
                                echo '<td>' ?> <i class="<?php echo userFavoritedThis($product_id) ? 'bi bi-star-fill' : 'bi bi-star' ?>" id="<?php echo $product_id ?>">Add to Favorites</i>
                                <?php 
                                echo "</td>";
                                echo "</tr>";
                            }

                            
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<?php 

include "includes/footer.php";

?>

<script>

$(document).ready(function(){

let user_id = <?php echo loggedInUser(); ?>

$('i').click(function(){

    $clicked_btn = $(this);
    let product_id = $(this).attr("id");

    if($clicked_btn.hasClass('bi bi-star')) {
        $clicked_btn.removeClass('bi bi-star');
        $clicked_btn.addClass('bi bi-star-fill');
        alert("Successfully Added to Favorites");

        $.ajax({
            url: "index.php",
            type: 'POST',
            data: {
                'favorite': 1, 
                'product_id': product_id,
                'user_id': user_id
            }
        });
    } else if($clicked_btn.hasClass('bi bi-star-fill')) {
        $clicked_btn.removeClass('bi bi-star-fill');
        $clicked_btn.addClass('bi bi-star');
        alert("Successfully Removed from Favorites");

        $.ajax({
            url: "index.php",
            type: 'POST', 
            data: {
                'unfavorite': 1, 
                'product_id': product_id, 
                'user_id': user_id
            }
        });
    }

});

});

</script>